import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Photo } from '../models/photo';
import { Album } from '../models/album';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private http: HttpClient) {
  }

  private get photosEndpoint(): string {
    return `${environment.serviceHost}/photos`;
  }


  /**********************************************************

              Start with the UsersService

  ************************************************************

































  */

  public getAlbumPhotos(album: Album) {
    const params = {
      albumId: `${album.id}`
    };
    return this.http.get<Photo[]>(this.photosEndpoint, {params});
  }

  public createAlbum(photo: Partial<Photo>) {
    return this.http.post<Photo>(this.photosEndpoint, photo);
  }

  public updateAlbum(photo: Photo) {
    return this.http.put<Photo>(`${this.photosEndpoint}/${photo.id}`, photo);
  }

  public deleteAlbum(photo: Photo) {
    return this.http.delete<void>(`${this.photosEndpoint}/${photo.id}`);
  }

}
