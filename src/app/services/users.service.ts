import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) {
  }

  private get usersEndpoint(): string {
    return `${environment.serviceHost}/users`;
  }

  // get all users

  // create user

  // get user

  // update user

  // delete user

}
