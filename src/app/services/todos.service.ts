import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Todo } from '../models/todo';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private http: HttpClient) {
  }

  private get todosEndpoint(): string {
    return `${environment.serviceHost}/posts`;
  }


  /**********************************************************

              Start with the UsersService

  ************************************************************

































  */

  public getAllTodos() {
    return this.http.get<Todo[]>(this.todosEndpoint);
  }

  public getUserTodos(user: User) {
    const params = {
      userId: `${user.id}`
    };
    return this.http.get<Todo[]>(this.todosEndpoint, {params});
  }

  public createTodo(todo: Partial<Todo>) {
    return this.http.post<Todo>(this.todosEndpoint, todo);
  }

  public updateTodo(todo: Todo) {
    return this.http.put<Todo>(`${this.todosEndpoint}/${todo.id}`, todo);
  }

  public deleteTodo(todo: Todo) {
    return this.http.delete<void>(`${this.todosEndpoint}/${todo.id}`);
  }

}
