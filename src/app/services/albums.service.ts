import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Album } from '../models/album';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  constructor(private http: HttpClient) {
  }

  private get albumsEndpoint(): string {
    return `${environment.serviceHost}/albums`;
  }


  /**********************************************************

              Start with the UsersService

  ************************************************************

































  */

  public getUserAlbums(user: User) {
    const params = {
      userId: `${user.id}`
    };
    return this.http.get<Album[]>(this.albumsEndpoint, {params});
  }

  public createAlbum(album: Partial<Album>) {
    return this.http.post<Album>(this.albumsEndpoint, album);
  }

  public updateAlbum(album: Album) {
    return this.http.put<Album>(`${this.albumsEndpoint}/${album.id}`, album);
  }

  public deleteAlbum(album: Album) {
    return this.http.delete<void>(`${this.albumsEndpoint}/${album.id}`);
  }

}
