import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Comment } from '../models/comment';
import { Post } from '../models/post';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) {
  }

  private get commentsEndpoint(): string {
    return `${environment.serviceHost}/comments`;
  }


  /**********************************************************

              Start with the UsersService

  ************************************************************

































  */

  public getUserComments(user: User) {
    const params = {
      userId: `${user.id}`
    };
    return this.http.get<Comment[]>(this.commentsEndpoint, {params});
  }

  public getPostComments(post: Post) {
    const params = {
      postId: `${post.id}`
    };
    return this.http.get<Comment[]>(this.commentsEndpoint, {params});
  }

  public createComment(comment: Partial<Comment>) {
    return this.http.post<Comment>(this.commentsEndpoint, comment);
  }

  public updateComment(comment: Comment) {
    return this.http.put<Comment>(`${this.commentsEndpoint}/${comment.id}`, comment);
  }

  public deleteComment(comment: Comment) {
    return this.http.delete<void>(`${this.commentsEndpoint}/${comment.id}`);
  }

}
