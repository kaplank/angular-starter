import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Post } from '../models/post';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) {
  }

  private get postsEndpoint(): string {
    return `${environment.serviceHost}/posts`;
  }


  /**********************************************************

              Start with the UsersService

  ************************************************************

































  */

  public getAllPosts() {
    return this.http.get<Post[]>(this.postsEndpoint);
  }

  public getUserPosts(user: User) {
    const params = {
      userId: `${user.id}`
    };
    return this.http.get<Post[]>(this.postsEndpoint, {params});
  }

  public createPost(post: Partial<Post>) {
    return this.http.post<Post>(this.postsEndpoint, post);
  }

  public updatePost(post: Post) {
    return this.http.put<Post>(`${this.postsEndpoint}/${post.id}`, post);
  }

  public deletePost(post: Post) {
    return this.http.delete<void>(`${this.postsEndpoint}/${post.id}`);
  }

}
